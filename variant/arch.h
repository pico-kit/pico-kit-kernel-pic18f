/*
 * Neon
 * Copyright (C)  2018  REAL-TIME CONSULTING
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/** @file
 *  @author      Nenad Radulovic
 *  @brief       Variant architecture for PIC18 header
 *
 *  @addtogroup  port
 *  @{
 */
/** @defgroup    port_pic18_variant_arch Variant architecture for PIC18
 *  @brief       Variant architecture for PIC18.
 *  @{
 */
/*---------------------------------------------------------------------------*/


#ifndef NEON_PIC18_VARIANT_ARCH_H_
#define NEON_PIC18_VARIANT_ARCH_H_

#include <xc.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NARCH_ID "pic18"
#define NARCH_PIC18 1
    
#define narch__cpu_stop()           for (;;)
    
#define narch__cpu_wait()           SLEEP()

#define narch__set_bit(u8_data, u8_bit)                                     \
    switch (u8_bit) {                                                       \
        case 0:                                                             \
            *u8_data |= 0x1 << 0;                                            \
            break;                                                          \
        case 1:                                                             \
            *u8_data |= 0x1 << 1;                                            \
            break;                                                          \
        case 2:                                                             \
            *u8_data |= 0x1 << 2;                                            \
            break;                                                          \
        case 3:                                                             \
            *u8_data |= 0x1 << 3;                                            \
            break;                                                          \
        case 4:                                                             \
            *u8_data |= 0x1 << 4;                                            \
            break;                                                          \
        case 5:                                                             \
            *u8_data |= 0x1 << 5;                                            \
            break;                                                          \
        case 6:                                                             \
            *u8_data |= 0x1 << 6;                                            \
            break;                                                          \
        case 7:                                                             \
            *u8_data |= 0x1 << 7;                                            \
            break;                                                          \
    }
    
#define narch__clear_bit(u8_data, u8_bit)                                   \
    do {                                                                    \
        *(u8_data) &= narch__inv_exp2(u8_bit);                              \
    } while (0)
    
#define narch__exp2(u8_x)           ng_exp2_table[u8_x]
        
#define narch__inv_exp2(u8_x)       ng_inv_exp2_table[u8_x]
    
#define narch__log2(u8_x)           ng_log2_table[u8_x]
    
extern const uint_fast8_t ng_exp2_table[8];

extern const uint_fast8_t ng_inv_exp2_table[8];

extern const uint_fast8_t ng_log2_table[256];

#ifdef __cplusplus
}
#endif

/** @} */
/** @} */
/*---------------------------------------------------------------------------*/
#endif /* NEON_PIC18_VARIANT_ARCH_H_ */
